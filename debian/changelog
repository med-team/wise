wise (2.4.1-25) unstable; urgency=medium

  * Team upload.
  * gcc-14.patch: fix multiple build failures with gcc 14.
    Note that the patch alone is not sufficient as such as motifmatrix.c
    is still affected by a case of assignment to integer from pointer
    without a cast which feels like a compiler bug.  See #1075638.
  * d/control: build depends on the libfl-dev.
    Apparently the missing libl.a went below the radar for building dyc.
  * d/rules: build and provide the dyc compiler to the rest of the build.
  * d/rules: work around weird compiler behavior. (Closes: #1075638)
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Thu, 15 Aug 2024 12:15:41 +0200

wise (2.4.1-24) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * watch file standard 4 (routine-update)
  * d/patches: Fix implicit-function-declaration errors (Closes: #1066705)
  * d/patches: Mark patch Forwarded fields as not-needed, since no upstream
    bug tracking, and update spelling.patch
  * d/patches: Fix executable-not-elf-or-script lintian warning
  * d/watch: Remove debian uupdate and access secure uri

 -- Lance Lin <lq27267@gmail.com>  Mon, 18 Mar 2024 22:32:37 +0700

wise (2.4.1-23) unstable; urgency=medium

  * Team upload.
  * Do not hard code the build architecture pkg-config
    Closes: #958094

 -- Helmut Grohne <helmut@subdivi.de>  Sat, 18 Apr 2020 17:57:01 +0200

wise (2.4.1-22) unstable; urgency=medium

  [ Helmut Grohne ]
  * Improve cross building: Let dh_auto_build pass cross tools to make.
    (Closes: #956279)

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Wrap long lines in changelog entries: 2.4.0-1.
  * Use secure URI in Homepage field.
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Fix permissions of *.hmm files
  * Fix autopkgtest since data files are not compressed any more

 -- Andreas Tille <tille@debian.org>  Fri, 17 Apr 2020 15:51:34 +0200

wise (2.4.1-21) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Long description for all packages
  * Drop unneeded Testsuite: autopkgtest

 -- Andreas Tille <tille@debian.org>  Thu, 04 Oct 2018 14:49:55 +0200

wise (2.4.1-20) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.0 (no changes needed)
  * d/rules: do not parse d/changelog
  * spelling

 -- Andreas Tille <tille@debian.org>  Fri, 22 Sep 2017 10:37:28 +0200

wise (2.4.1-19) unstable; urgency=medium

  * Team upload.
  * Make build reproducible under build path variation.
    - Omit the PDF trailer ID as it is based on the original input path.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 31 Aug 2016 22:28:34 +0000

wise (2.4.1-18) unstable; urgency=medium

  * Team upload.
  * Make build reproducible.
    - Fix dates in manual .tex files.
    - Use stable order for API inputs.
  * Bump Standards-Version.
  * Enable full hardening.
  * Use secure Vcs-Browser.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Thu, 19 May 2016 21:53:30 +0000

wise (2.4.1-17) unstable; urgency=medium

  * debian/tests/control: explicitly depend on wise.  This avoids that
    ‘sadt’ skips the test when wise-doc is not installed.
  * debian/tests/run-unit-test: skip the first line of testman.pl, that
    misses a proper shebang and trigger a failure.
  * debian/control: shorten synopsis to less than 72 characters.
  * Conforms with Policy 3.9.6.

 -- Charles Plessy <plessy@debian.org>  Tue, 23 Sep 2014 20:14:47 +0900

wise (2.4.1-16) unstable; urgency=medium

  * Make sure package builds also for arch only builds
    Closes: #757275

 -- Andreas Tille <tille@debian.org>  Thu, 07 Aug 2014 09:49:11 +0200

wise (2.4.1-15) unstable; urgency=medium

  * Move debian/upstream to debian/upstream/metadata
  * debian/control:
     - add myself to Uploaders
     - cme fix dpkg-control
     - debhelper 9
     - remove unneeded Build-Depends
  * debian/README.source: removed in favour of DEP5 headers
  * Make sure dnal has a valid return value (this affects the test suite
    of python-biopython on some architectures - see #751277)
  * debian/rules:
     - drop useless xz compression
     - add dh_auto_test
  * debian/patches/10_fix_path_to_data_files.patch: Brute force patch to
    fix the PATH to the data files to make the tools finding the needed
    data for the test suite

 -- Andreas Tille <tille@debian.org>  Mon, 04 Aug 2014 19:25:58 +0200

wise (2.4.1-14) unstable; urgency=low

  * Fixed FTBFS bug (Closes: #716983).

 -- Philipp Benner <philipp@debian.org>  Mon, 15 Jul 2013 18:22:15 +0200

wise (2.4.1-13) unstable; urgency=low

  * Added mayhem bugfixes.
  * Moved examples/ from wise-doc to wise-data package.
  * Removed -O3 cflag from makefiles.

 -- Philipp Benner <philipp@debian.org>  Sun, 07 Jul 2013 13:33:01 +0200

wise (2.4.1-12) unstable; urgency=low

  * Added missing -D_FORTIFY_SOURCE=2 for hardening support
  * Standards version 3.9.4

 -- Philipp Benner <philipp@debian.org>  Mon, 28 Jan 2013 20:33:16 +0100

wise (2.4.1-11) unstable; urgency=low

  [ Andreas Tille ]
  * debian/upstream: Add publication data

  [ Philipp Benner ]
  * Added hardening flags

 -- Philipp Benner <philipp@debian.org>  Sun, 27 Jan 2013 18:05:31 +0100

wise (2.4.1-10) unstable; urgency=low

  * Convert debian/rules to dh, Closes: #666319, as it restores build flags.
  * Verbose debhelper in debian/rules.
  * Normalised and upgraded debian/copyright file with ‘cme fix’.
  * Pre-Depends on dpkg (>= 1.15.6~) (debian/control).
  * Conforms to Debian policy 3.9.3 (debian/control, no other changes needed).
  * Convert debian/rules to dh, Closes: #666319, as it restores build flags.
  * Documented metadata in debian/upstream.

 -- Charles Plessy <plessy@debian.org>  Fri, 30 Mar 2012 19:25:53 +0900

wise (2.4.1-9) unstable; urgency=low

  * Correct src/models/makefile to link --as-needed (Closes: #641333).
  * Conforms with Policy 3.9.2 (debian/control, no other changes needed).
  * Corrected VCS URLs (debian/control).
  * Use Debhelper 8 (debian/compat, debian/control).
  * Added build-arch and build-indep targets to debian/rules.
  * Machine-readable debian/copryight file conforms to DEP 5.
  * Compress binary package with xz (debian/rules).

 -- Charles Plessy <plessy@debian.org>  Wed, 14 Sep 2011 17:52:32 +0900

wise (2.4.1-8) unstable; urgency=low

  * Fixed conflicting definitions of getline(). (Closes: #552821)
  * Added README.source.
  * Standards version 3.8.3.
  * Fixed spelling error in package description.

 -- Philipp Benner <philipp@debian.org>  Sat, 31 Oct 2009 21:25:52 +0000

wise (2.4.1-7) unstable; urgency=low

  [ Steffen Moeller ]
  * Now builds with glib-2. (Closes: #523715). Special thank to Barry deFreese.
  * Updated policy to 3.8.1 (no changes required).
  * fixed api->API lintian warning.

  [ Charles Plessy ]
  * debian/control:
    - Added myself to the Uploaders.
    - Added ${misc:Depends} to the package dependencies.
    - Using Debhelper 7 (also debian/compat).
    - Eliminated redundancy between the long descriptions of wise and wise-doc.
  * Replaced ‘dh_clean -k’ by ‘dh_prep’ in debian/rules.

 -- Charles Plessy <plessy@debian.org>  Mon, 15 Jun 2009 20:49:27 +0900

wise (2.4.1-6) unstable; urgency=low

  * Updated policy to 3.8.0 (no changes required).
  * wise now suggests wise-doc (downgraded from 'recommends').
  [ Steffen Moeller ]
  * Using quilt instead of dpatch.
  [ Philipp Benner ]

 -- Philipp Benner <pbenner@uni-osnabrueck.de>  Sun, 25 Jan 2009 22:57:27 +0000

wise (2.4.1-5) unstable; urgency=low

  * Removed a bashism in debian/rules (Closes: #478639).

 -- Charles Plessy <plessy@debian.org>  Sun, 04 May 2008 15:19:01 +0900

wise (2.4.1-4) unstable; urgency=low

  * Changed the doc-base section according to the new policy
    [Charles Plessy].

 -- Philipp Benner <pbenner@uni-osnabrueck.de>  Mon, 31 Mar 2008 10:07:35 +0200

wise (2.4.1-3) unstable; urgency=low

  * New maintainer e-mail address.
  * debian/control: Added DM-Upload-Allowed: yes.

 -- Philipp Benner <pbenner@uni-osnabrueck.de>  Mon, 14 Jan 2008 18:33:27 +0100

wise (2.4.1-2) unstable; urgency=low

  * Using official Vcs-Browser and Vcs-Svn fields (debian/control).
  * Moved the Homepage: field out from the package's description
  + (Charles Plessy).
  * Converted copyright file to machine interpretable format.
  * Changed maintainer to Debian-Med Packaging Team.

 -- Philipp Benner <mail@philipp-benner.de>  Sun, 02 Dec 2007 16:15:50 +0100

wise (2.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Philipp Benner <mail@philipp-benner.de>  Sat, 15 Sep 2007 17:18:49 +0200

wise (2.4.0-3) unstable; urgency=low

  * Removed debtags from debian/control.
  * Updated package description (Closes: #435904).
  * Excluded genewise21.eps and genewise6.eps from documentation
  + (workaround for a bug in gs-esp).

 -- Philipp Benner <mail@philipp-benner.de>  Sun, 05 Aug 2007 20:21:35 +0200

wise (2.4.0-2) unstable; urgency=low

  * Converted wise2.tex to a pdflatex compatible latex file
  + (workaround for a bug in dvips)

 -- Philipp Benner <mail@philipp-benner.de>  Fri, 27 Jul 2007 18:46:33 +0200

wise (2.4.0-1) unstable; urgency=low

  * New upstream release (Closes: #431392)
  * New dpatch (02_isnumber): src/models/phasemodel.c: replaces isnumber() by
    isdigit()
  * New dpatch (03_doc-nodycache): docs/wise2.tex: minor bugfix

 -- Philipp Benner <mail@philipp-benner.de>  Tue, 24 Jul 2007 11:58:36 +0200

wise (2.2.0-5) unstable; urgency=low

  * Removed obsolete dependencies (Closes: #422123)

 -- Philipp Benner <mail@philipp-benner.de>  Sun,  6 May 2007 22:42:27 +0200

wise (2.2.0-4) unstable; urgency=low

  * Adapted debian-med tags
  * Uscan support
  * Migrating to texlive
  * use /bin/sh instead of /bin/csh in src/welcome.csh

 -- Philipp Benner <mail@philipp-benner.de>  Tue, 27 Mar 2007 19:29:08 +0200

wise (2.2.0-3) unstable; urgency=low

  * package tags
  * fixed typos in package description

 -- Philipp Benner <mail@philipp-benner.de>  Thu, 21 Sep 2006 14:22:39 +0200

wise (2.2.0-2) unstable; urgency=low

  * removed \ref tags from package description
  * moved examples to package wise-doc
  * fixed typo in README.Debian
  * minor changes in debian/control
  * fixed typo in package description (Closes: #380451)
  * new html documentation

 -- Philipp Benner <mail@philipp-benner.de>  Sun, 30 Jul 2006 14:08:20 +0200

wise (2.2.0-1) unstable; urgency=low

  * Initial release (Closes: #379604)

 -- Philipp Benner <mail@philipp-benner.de>  Sun, 23 Jul 2006 17:29:49 +0200
